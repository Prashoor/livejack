//
//  APICall.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 24/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "APICall.h"
#import "Reachability.h"

typedef struct RequestSetup {
     __unsafe_unretained NSString * methodName;
    __unsafe_unretained NSString * methodType;
    bool isURLAppended;
} HTTPRequestSetup;



static NSString * API_URL = @"http://8.26.21.243/";

@interface APICall ()

@end

@implementation APICall

- (void)callMethod:(APIMethod)method andParams:(NSDictionary *)params withCallBack:(void(^)(NSError* err,id response))callBack {
    if (![Reachability reachabilityForInternetConnection]) {
        return;
    }
    
    NSURLSessionConfiguration * config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:config];
    
    HTTPRequestSetup setup = [self.class methodWith:method];
    
    NSMutableString *uri = [NSMutableString stringWithFormat:@"%@%@",API_URL, setup.methodName];
    if (setup.isURLAppended) {
        [uri appendString:@"?"];
        NSMutableString * prstr = [NSMutableString string];
        NSEnumerator *tableIterator = [params keyEnumerator];
        NSString *key;
        while((key = [tableIterator nextObject]))
        {
            if (prstr.length != 0) {
                [prstr appendString:@"&"];
            }
            [prstr appendFormat:@"%@=%@", key, params[key]];
        }
        [uri appendString:prstr];
    }
    
    NSLog(@"URI Called : %@", uri);
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:uri]];
    
    if (!setup.isURLAppended) {
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSError *jerr;
        
        NSData * body = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&jerr];
        NSLog(@"%@", [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding]);
        [request setHTTPBody:body];
    }
    
    [request setHTTPMethod:setup.methodType];
    
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (data.length == 0) {
                callBack(error, @"No response");
            }
            else if (error) {
                NSString * resp = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"%@", resp);
                callBack(error, resp);
            }
            else {
                NSError * err;
                id resp = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
                if ([resp[@"success"] boolValue]) {
                    callBack(err, resp);
                }
                else {
                    callBack([[NSError alloc] initWithDomain:@"API Call failed" code:256 userInfo:nil], [NSString stringWithFormat:@"%@", resp]);
                }
            }
        });
    }];
    [task resume];
}


+ (HTTPRequestSetup)methodWith:(APIMethod) method {
    
    HTTPRequestSetup methodDescript;
    
    switch (method) {
        
        case LoginUser:
            methodDescript.methodName = @"userLogin";
            methodDescript.methodType =  @"POST";
            methodDescript.isURLAppended = false;
            break;
            
        case RegisterUser:
            methodDescript.methodName = @"registerUser";
            methodDescript.methodType =  @"POST";
            methodDescript.isURLAppended = false;
            break;
            
        case GetGroupStatus:
            methodDescript.methodName =  @"getStreams";
            methodDescript.methodType =  @"GET";
            methodDescript.isURLAppended = true;
            break;
        
        default:
            break;
    }
    return methodDescript;
}

@end
