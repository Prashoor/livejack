//
//  AppDelegate.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 09/06/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

