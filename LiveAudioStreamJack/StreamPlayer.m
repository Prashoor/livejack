//
//  StreamPlayer.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 17/06/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "StreamPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "BroadcastStreamClient.h"
//#import "MediaStreamPlayer.h"
//#import "MPMediaDecoder.h"
#import "RTSPPlayerPro.h"

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@interface StreamPlayer () <MPIMediaStreamEvent>
@property (readonly) AVAudioSession * session;
@property (readonly, getter=plItem) NSMutableArray<RTSPPlayerPro *> *plItem;

@property (readonly) NSMutableArray<AVPlayer *> * listAV;
@end

@implementation StreamPlayer

- (NSMutableArray<RTSPPlayerPro *> *)plItem {
    static NSMutableArray<RTSPPlayerPro *> * list;
    if (!list) {
        list = [[NSMutableArray<RTSPPlayerPro *> alloc] init];
    }
    return list;
}

- (void) startPlayer:(NSArray<NSString *> *)streams {
    // Do any additional setup after loading the view, typically from a nib.
    for (NSString * streamURL in streams) {
        RTSPPlayerPro * player = [[RTSPPlayerPro alloc] init];
        [player setMediaURL:[NSURL URLWithString:[NSString stringWithFormat:@"rtmp://%@/%@/%@",STREAM,APP, streamURL]]];
        [self.plItem addObject:player];
    }
}

- (void)stopPlayer {
    for (RTSPPlayerPro * decoder in self.plItem) {
//        [decoder cleanupStream];
    }
}

+ (RTMPClient *)getSocket {
    RTMPClient *socket = [[RTMPClient alloc] init:[NSString stringWithFormat:@"rtmp://%@/%@",STREAM,APP]];
    if (!socket) {
        NSLog(@"Connection has not be created");
        return nil;
    }
    [socket spawnSocketThread];
    return socket;
}

- (void) sendTo:(NSString *)streamName {
    RTMPClient *socket = [self.class getSocket];
    
    if (!socket) {
        return;
    }
    
    MPVideoResolution resolution = RESOLUTION_VGA;
    BroadcastStreamClient *upstream = [[BroadcastStreamClient alloc] initWithClient:socket resolution:resolution];
    upstream.delegate = self;
    upstream.videoCodecId = MP_VIDEO_CODEC_NONE;
    upstream.audioCodecId = MP_AUDIO_CODEC_AAC;
    [upstream stream:streamName publishType:PUBLISH_LIVE];
}

#pragma mark MPIMediaStreamEvent Methods

-(void)stateChanged:(id)sender state:(MPMediaStreamState)state description:(NSString *)description {
    
    NSLog(@" $$$$$$ <MPIMediaStreamEvent> stateChangedEvent: %d = %@ [%@]", (int)state, description, [NSThread isMainThread]?@"M":@"T");
    
    switch (state) {
            
        case STREAM_CREATED: {
            break;
        }
            
        case STREAM_PAUSED: {
            if ([description isEqualToString:MP_NETSTREAM_PLAY_STREAM_NOT_FOUND]) {
                break;
            }
            break;
        }
            
        case STREAM_PLAYING: {
            if ([description isEqualToString:MP_RESOURCE_TEMPORARILY_UNAVAILABLE]) {
                break;
            }
            if ([description isEqualToString:MP_NETSTREAM_PLAY_STREAM_NOT_FOUND]) {
                break;
            }
            [MPMediaData routeAudioToSpeaker];
            break;
        }
            
        default:
            break;
    }
}

-(void)connectFailed:(id)sender code:(int)code description:(NSString *)description {
    
    NSLog(@" $$$$$$ <MPIMediaStreamEvent> connectFailedEvent: %d = %@ [%@]", code, description, [NSThread isMainThread]?@"M":@"T");
}

-(void)metadataReceived:(id)sender event:(NSString *)event metadata:(NSDictionary *)metadata {
    NSLog(@" $$$$$$ <MPIMediaStreamEvent> dataReceived: EVENT: %@, METADATA = %@ [%@]", event, metadata, [NSThread isMainThread]?@"M":@"T");
}

@end
