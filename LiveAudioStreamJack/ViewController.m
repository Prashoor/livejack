//
//  ViewController.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 09/06/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "ViewController.h"
#import "StreamPlayer.h"
#import "AlertBox.h"
@interface StreamStore: NSObject
@property (retain) NSString * streamName;

@end

@implementation StreamStore
@synthesize streamName;

@end


@interface ViewController ()
@property (nonatomic, strong) StreamPlayer * player;
@property (nonatomic, readonly) NSMutableArray<NSString *> * allStreams;
@property (weak, nonatomic) IBOutlet UITextField *streamDomain;
@property (weak, nonatomic) IBOutlet UITextField *appName;
@property (weak, nonatomic) IBOutlet UITextField *streamName;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _player = [[StreamPlayer alloc] init];
    
    _streamDomain.text= @"8.26.21.243";
    _appName.text = @"live";
}

- (NSMutableArray<NSString *> *)allStreams {
    static NSMutableArray<NSString *> *streams = nil;
    if (!streams) {
        streams = [[NSMutableArray<NSString *>  alloc] init];
    }
    return streams;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelled:(UITextField *)sender {
    [sender endEditing:YES];
}

- (IBAction)startPlayer:(id)sender {
    [_player startPlayer:self.allStreams];
}

- (IBAction)addStream:(UIButton *)sender {
    
    NSString * streamname = _streamName.text;
    
    if (streamname.length == 0) {
        [AlertBox showAlert:@"All Fields Are compulsary" confirm:nil];
        return;
    }
    _streamName.text = @"";
    [AlertBox showAlert:@"Stream Added" confirm:nil];
    
    [self.allStreams addObject:streamname];
    //[NSURL URLWithString:[NSString stringWithFormat:@"rtmp://%@:1935/live/%@",streamDm, streamname]]
}

- (IBAction)stopPlayer:(id)sender {
    [_player stopPlayer];
    [self.allStreams removeAllObjects];
}
@end
