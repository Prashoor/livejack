//
//  AudioListViewController.m
//  LiveStreamJack
//
//  Created by Nipun Vyas on 12/06/16.
//  Copyright © 2016 Nipun Vyas. All rights reserved.
//

#import "AudioListViewController.h"
#import "FAKIcon.h"
#import "FAKIonIcons.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@interface AudioListViewController ()<UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate>
{
    IBOutlet UITableView *audioListView;
    NSArray *audioArr;
    IBOutlet NSLayoutConstraint *hgt;
    IBOutlet UISlider *timeSlider;
    IBOutlet UISlider *volumeSlider;
    IBOutlet UILabel  *startTym;
    IBOutlet UILabel  *endTym;
    IBOutlet UIButton *playBtn;
    IBOutlet UIButton *nextAudioBtn;
    IBOutlet UIButton *preAudioBtn;
    IBOutlet UIButton *backBtn;
    AVAudioPlayer *audioPlayer;
    NSTimer *sliderUpdateTimer;
    BOOL isPlaying;
    NSInteger selectedIndex;
    NSString *_selectedAudioFileName, *lastSelectedFile;
}
@end

@implementation AudioListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = YES;
    
    NSBundle *bundle = [[NSBundle mainBundle] initWithPath:DOCUMENTS_FOLDER];
    
    audioArr = @[];
    
    // Set PayPause Button
    FAKIcon *playIcon=[FAKIonIcons iosPlayIconWithSize:playBtn.frame.size.width];
    FAKIcon *pauseIcon=[FAKIonIcons iosPauseIconWithSize:playBtn.frame.size.width];
    FAKIcon *fastForwardIcon=[FAKIonIcons iosFastforwardIconWithSize:nextAudioBtn.frame.size.width];
    FAKIcon *reWindIcon=[FAKIonIcons iosRewindIconWithSize:preAudioBtn.frame.size.width];
    
    [playIcon addAttribute:NSForegroundColorAttributeName  value:[UIColor whiteColor]];
    [pauseIcon  addAttribute:NSForegroundColorAttributeName  value:[UIColor whiteColor]];
    [fastForwardIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    [reWindIcon addAttribute:NSForegroundColorAttributeName  value:[UIColor whiteColor]];

    [playBtn setImage:[playIcon  imageWithSize:playBtn.frame.size] forState:UIControlStateNormal];
    [playBtn setImage:[pauseIcon imageWithSize:playBtn.frame.size] forState:UIControlStateSelected];
    [playBtn setTitle:@"" forState:UIControlStateNormal];
    
    [nextAudioBtn setImage:[fastForwardIcon imageWithSize:nextAudioBtn.frame.size] forState:UIControlStateNormal];
    [nextAudioBtn setTitle:@"" forState:UIControlStateNormal];
    
    [preAudioBtn setImage:[reWindIcon imageWithSize:preAudioBtn.frame.size] forState:UIControlStateNormal];
    [preAudioBtn setTitle:@"" forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    hgt.constant = 0.f;
    
    NSError *error = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    [audioSession setActive: YES error: &error];
    
    UITapGestureRecognizer *timerTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(timeSliderTapped:)];
    [timeSlider addGestureRecognizer:timerTapGestureRecognizer];
    
    UITapGestureRecognizer *volumeTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(volumeSliderTapped:)];
    [volumeSlider addGestureRecognizer:volumeTapGestureRecognizer];
    
}

- (IBAction)backToPreviousView:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)timeSliderTapped:(UIGestureRecognizer *)gestureRecognizer {
    timeSlider = (UISlider*)gestureRecognizer.view;
    if (timeSlider.highlighted)
        return;
    CGPoint pt = [gestureRecognizer locationInView: timeSlider];
    CGFloat percentage = pt.x / timeSlider.bounds.size.width;
    CGFloat delta = percentage * (timeSlider.maximumValue - timeSlider.minimumValue);
    CGFloat value = timeSlider.minimumValue + delta;
    [timeSlider setValue:value animated:YES];
    
    [self changeSliderValue:timeSlider];
}

- (void)volumeSliderTapped:(UIGestureRecognizer *)gestureRecognizer {
    volumeSlider = (UISlider*)gestureRecognizer.view;
    if (volumeSlider.highlighted)
        return; // tap on thumb, let slider deal with it
    CGPoint pt = [gestureRecognizer locationInView: volumeSlider];
    CGFloat percentage = pt.x / volumeSlider.bounds.size.width;
    CGFloat delta = percentage * (volumeSlider.maximumValue - volumeSlider.minimumValue);
    CGFloat value = volumeSlider.minimumValue + delta;
    [volumeSlider setValue:value animated:YES];
    [self changeVolume:volumeSlider];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return audioArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *audioCell = [tableView dequeueReusableCellWithIdentifier:@"audioCell"];
    if (!audioCell) {
        audioCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"audioCell"];
    }
    audioCell.textLabel.text = [audioArr objectAtIndex:indexPath.row];
    audioCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return audioCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    hgt.constant = 123;
    
    _selectedAudioFileName = [audioArr objectAtIndex:indexPath.row];
    
    
    selectedIndex = indexPath.row;
    
    [self startAudioPlayerOnSelection];
}


- (void)startAudioPlayerOnSelection {
    
    if (_selectedAudioFileName != lastSelectedFile) {
        
        [self preparePlayer:_selectedAudioFileName];
        if (!lastSelectedFile) {
            [timeSlider setThumbImage:[UIImage imageNamed:@"thumbImg.png"] forState:UIControlStateNormal];
        }
    }
    [self playPlayer];
    
    lastSelectedFile = _selectedAudioFileName;
    
    if (selectedIndex == audioArr.count - 1) {
        nextAudioBtn.userInteractionEnabled = NO;
    }
    else if (selectedIndex == 0) {
        preAudioBtn.userInteractionEnabled = NO;
    }
}

#pragma mark - Player Controls

- (void) preparePlayer:(NSString *)audioName {
    if (audioPlayer) {
        [self stopPlayer];
    }
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[audioName stringByReplacingOccurrencesOfString:@".mp3" withString:@""] ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    
    [self setAudioPlayerValue];
}

- (void)setAudioPlayerValue {
    audioPlayer.numberOfLoops = 0;
    audioPlayer.delegate   = self;
    audioPlayer.enableRate = YES;
    [audioPlayer prepareToPlay];
    
    timeSlider.minimumValue = 0;
    timeSlider.maximumValue = audioPlayer.duration-1;
    
    [self updateSlider];
    [self startUpdateTimer];
}

- (void) startUpdateTimer {
    if (sliderUpdateTimer == nil) {
        sliderUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        [sliderUpdateTimer fire];
    }
}

- (void) stopUpdateTimer {
    [sliderUpdateTimer invalidate];
    sliderUpdateTimer = nil;
}

- (void) playPlayer {
    if (audioPlayer) {
        isPlaying = YES;
        playBtn.selected = YES;
        [audioPlayer play];
        [self startUpdateTimer];
    }
}

- (void) pausePlayer {
    if (audioPlayer) {
        isPlaying = NO;
        playBtn.selected = NO;
        [audioPlayer pause];
        [self stopUpdateTimer];
    }
}

- (void) stopPlayer {
    if (audioPlayer) {
        isPlaying = NO;
        playBtn.selected = NO;
        [audioPlayer stop];
        audioPlayer = nil;
        
        [self stopUpdateTimer];
    }
}

- (void) updateSlider {
    [self updateTimeLabels];
    timeSlider.value = audioPlayer.currentTime;
}

- (void) updateTimeLabels {
    startTym.text   = [self stringFromTimeInterval:audioPlayer.currentTime];
    endTym.text = [NSString stringWithFormat:@"-%@", [self stringFromTimeInterval:(audioPlayer.duration - audioPlayer.currentTime)]];
    if (audioPlayer.currentTime == timeSlider.minimumValue) {
//        if (isRepeated && rzepeatModeBtn.selected && lsatAudio.isRepeatModeSelected) {
//            if (selectedIndex == audioNameArr.count - 1) {
//                return;
//            }
//            [self playNextAudio:nextAudioBtn];
//        }
    }
//    if (audioPlayer.currentTime > timeSlider.minimumValue && repeatModeBtn.selected && lsatAudio.isRepeatModeSelected) {
//        isRepeated = YES;
//    }
}

- (NSString *) stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

- (IBAction)playStopAudio:(UIButton *)sender {
    if (playBtn.selected) {
        [self pausePlayer];
    } else {
        [self playPlayer];
        
    }
}

- (IBAction)previousAuio:(id)sender {
    if (selectedIndex == 1) {
        preAudioBtn.userInteractionEnabled = NO;
    }
    if (selectedIndex == 0) {
        return;
    }
    selectedIndex--;
    nextAudioBtn.userInteractionEnabled = YES;
    NSString *fileName = [audioArr objectAtIndex:selectedIndex];
    [self preparePlayer:fileName];
    [self playPlayer];
}

- (IBAction)nextAudio:(id)sender {
    if (selectedIndex == audioArr.count - 2) {
        nextAudioBtn.userInteractionEnabled = NO;
    }
    if (selectedIndex == audioArr.count - 1) {
        return;
    }
    selectedIndex++;
    preAudioBtn.userInteractionEnabled = YES;
    NSString *fileName = [audioArr objectAtIndex:selectedIndex];
    [self preparePlayer:fileName];
    
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC);
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        [self playPlayer];
    });
}

- (IBAction)changeVolume:(UISlider *)sender {
    audioPlayer.volume = sender.value;
}

- (IBAction) changeSliderValue:(UISlider *)sender {
    audioPlayer.currentTime = sender.value;
    [self updateTimeLabels];
}

#pragma mark - AudioPlayer Delegates

- (void) audioPlayerBeginInterruption:(AVAudioPlayer *)player {
    playBtn.selected = NO;
}

- (void) audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags {
    if (player.play) {
        playBtn.selected = YES;
    }
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    playBtn.selected = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
