//
//  DesignButton.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 13/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface DesignButton : UIButton
@property (nonatomic) IBInspectable UIColor * backColor;
@end
