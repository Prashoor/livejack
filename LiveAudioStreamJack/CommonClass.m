//
//  CommonClass.m
//  LiveAudioStreamJack
//
//  Created by Nipun Vyas on 25/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "CommonClass.h"

@implementation CommonClass

+ (CommonClass *)sharedInstance {
    static CommonClass *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

@end
