//
//  User.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 29/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic) NSString * userID;
@property (nonatomic) NSString * firstName;
@property (nonatomic) NSString * lastName;
@property (nonatomic) NSNumber * groupid;
@property (nonatomic) NSString * email;

+ (instancetype)myProfile;
- (void)saveMyProfile:(NSDictionary *)data;
- (void)setUserProfile:(NSDictionary *)prof;

@end
