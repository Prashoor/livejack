//
//  LoginViewController.m
//  LiveAudioStreamJack
//
//  Created by Nipun Vyas on 24/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "LoginViewController.h"
#import "APICall.h"
#import "CommonClass.h"
#import "User.h"

@interface LoginViewController ()
{
    IBOutlet UITextField *userID;
    IBOutlet UITextField *password;
    
    IBOutlet UIButton *loginBtn;
    IBOutlet UIButton *signupBtn;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([User myProfile].userID) {
        [self goToHome];
    }
    loginBtn.layer.cornerRadius = 8.0;
    loginBtn.layer.borderColor  = [UIColor whiteColor].CGColor;
    loginBtn.layer.borderWidth  = 2.0;
    
    signupBtn.layer.cornerRadius = 8.0;
    signupBtn.layer.borderColor  = [UIColor whiteColor].CGColor;
    signupBtn.layer.borderWidth  = 2.0;
    
    if ([[CommonClass sharedInstance] userEmail].length > 0) {
        userID.text = [[CommonClass sharedInstance] userEmail];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)userLogin:(id)sender {
    NSString *msg = @"";
    if (userID.text.length == 0) {
        if (msg.length == 0) {
            msg = @"Please enter valid user id";
        }
        else {
            msg = [NSString stringWithFormat:@"%@, user id", msg];
        }
    }
    if (password.text.length == 0) {
        if (msg.length == 0) {
            msg = @"Please enter valid password";
        }
        else {
            msg = [NSString stringWithFormat:@"%@, password", msg];
        }
    }
    
    if (msg.length != 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    APICall *helper = [[APICall alloc] init];
    
    
    NSMutableDictionary *userdict = [NSMutableDictionary dictionary];
    
    [userdict setObject:userID.text forKey:@"email"];
    [userdict setObject:password.text forKey:@"password"];
    [helper callMethod:LoginUser andParams:userdict withCallBack:^(NSError *err, id response) {
        if (!err) {
            [[User myProfile] saveMyProfile:response[@"userdata"]];
            [self goToHome];
        }
    }];
}

- (void)goToHome {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
