//
//  StreamPlayer.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 17/06/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * STREAM = @"8.26.21.243";
static NSString * APP = @"live";

@interface StreamPlayer : NSObject

- (void) startPlayer:(NSArray<NSString *> *)streams;
- (void)stopPlayer;

- (void) sendTo:(NSString *)streamName;

@end
