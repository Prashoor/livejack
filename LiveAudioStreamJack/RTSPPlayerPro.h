//
//  RTSPPlayerPro.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 18/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVPlayerTouch/AVPlayerTouch.h>

@interface RTSPPlayerPro : NSObject
// audio or video url (local or network)
@property (nonatomic, strong) NSURL *mediaURL;
@property (nonatomic, strong) NSString *avFormatName;

// did load video successfully callback.
@property (nonatomic, copy) void (^didLoadVideo)(FFAVPlayerController *player);
@property (nonatomic, copy) NSNumber* (^getStartTime)(FFAVPlayerController *player, NSURL *url);
@property (nonatomic, copy) void (^saveProgress)(FFAVPlayerController *player);
@property (nonatomic, copy) void (^didFinishPlayback)(FFAVPlayerController *player);
@property (nonatomic, copy) void (^willDismiss)(void);

- (void)startPlaybackAt:(NSNumber *)startTimePosition;
- (void)pause;
@end
