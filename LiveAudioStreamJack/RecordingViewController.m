//
//  RecordingViewController.m
//  LiveStreamJack
//
//  Created by Nipun Vyas on 12/06/16.
//  Copyright © 2016 Nipun Vyas. All rights reserved.
//

#import "RecordingViewController.h"
#import "StreamPlayer.h"
#import "APICall.h"
#import "StreamPlayer.h"
#import "AlertBox.h"
#import "User.h"

@interface RecordingViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>
{
    IBOutlet UICollectionView *recordingListView;
    IBOutlet UILabel *connectionLbl;
    IBOutlet UILabel *titleLbl;
    IBOutlet UIButton *startBtn;
    IBOutlet UIView *gradiantView;
    IBOutlet UIButton *backBtn;
    UIView *backVw;
    UITextField *titleFld;
    NSArray *userNmArr;
    
    NSIndexPath *idxPath;
    NSTimer *sliderUpdateTimer;
    APICall * call;
    NSDictionary * myStream;
}
@end

@implementation RecordingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    userNmArr = @[@"David", @"Joan", @"Roman", @"Zaltan", @"Frank"];
    call = [[APICall alloc] init];
    [call callMethod:GetGroupStatus andParams:@{@"userId": [User myProfile].userID} withCallBack:^(NSError *err, id response) {
        if (!err) {
            userNmArr = response[@"usersList"];
            if ([response[@"mystream"] isKindOfClass:[NSDictionary class]]) {
                myStream = response[@"mystream"];
            }
        }
    }];
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    backVw = [[UIView alloc] initWithFrame:self.view.bounds];
    backVw.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    
    UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 200)];
    vw.center = self.view.center;
    vw.backgroundColor = [UIColor whiteColor];
    vw.layer.cornerRadius = 10.f;
    
    UILabel *headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 150, 30)];
    headerLbl.text = @"Enter Title";
    headerLbl.textAlignment = NSTextAlignmentCenter;
    headerLbl.backgroundColor = [UIColor clearColor];
    
    titleFld = [[UITextField alloc] initWithFrame:CGRectMake(20, 50, 210, 30)];
    titleFld.backgroundColor = [UIColor whiteColor];
    titleFld.textColor = [UIColor blackColor];
    titleFld.layer.borderColor = [UIColor blackColor].CGColor;
    titleFld.layer.borderWidth = 0.8f;
    titleFld.layer.cornerRadius = 5.0f;
    titleFld.textAlignment = NSTextAlignmentCenter;
    titleFld.text = @"Untitled";
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(20, 110, 210, 30);
    saveBtn.backgroundColor = [UIColor lightGrayColor];
    saveBtn.layer.cornerRadius = 5.0f;
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveTitle) forControlEvents:UIControlEventTouchUpInside];

    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(20, 150, 210, 30);
    cancelBtn.backgroundColor = [UIColor darkGrayColor];
    cancelBtn.layer.cornerRadius = 5.0f;
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelTitle) forControlEvents:UIControlEventTouchUpInside];

    [vw addSubview:titleFld];
    [vw addSubview:headerLbl];
    [vw addSubview:saveBtn];
    [vw addSubview:cancelBtn];
    
    [backVw addSubview:vw];
    [self.view addSubview:backVw];
    connectionLbl.text = @"Online User  \t Offline User  \t Total User ";
}

- (IBAction)backToPreviousView:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    idxPath = indexPath;
    RecordingCell *rCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RecordingCell" forIndexPath:indexPath];
    rCell.connectedUserNm.text = [userNmArr objectAtIndex:indexPath.row];
    
    return rCell;
}

- (void) startUpdateTimer {
    if (sliderUpdateTimer == nil) {
        sliderUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(generateRandomNumbers) userInfo:nil repeats:YES];
        [sliderUpdateTimer fire];
    }
}


- (void)generateRandomNumbers {
    CGFloat low_bound = 0;
    CGFloat high_bound = 10;
    for (int i = 0; i < 5; i++) {
        CGFloat rndValue = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
        RecordingCell *cell = (RecordingCell *)[recordingListView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [cell maskForLevel:rndValue];
    }
}

- (void)stopUpdateTimer {
    [sliderUpdateTimer invalidate];
    sliderUpdateTimer = nil;
}

- (void)saveTitle {
   titleLbl.text = titleFld.text;
    [backVw setHidden:YES];
    [self userCount];
}

- (void)cancelTitle {
    [backVw setHidden:YES];
    [self userCount];
}

- (void)userCount {
    connectionLbl.text = @"Online User 3   Offline User 2   Total User 5";
}

- (IBAction)startRecording:(UIButton *)sender {
    NSInteger i;
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         gradiantView.backgroundColor = [UIColor redColor];
                         gradiantView.layer.shadowColor = [UIColor redColor].CGColor;
                         gradiantView.layer.shadowOpacity = 10.0f;
                     }
                     completion:^(BOOL finished) {
                     }];
    
    if (!startBtn.selected) {
        [startBtn setTitle:@"Stop Recording" forState:UIControlStateNormal];
        startBtn.selected = YES;
        
        [self startUpdateTimer];
        
        i = 0.5;
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             gradiantView.backgroundColor = [UIColor colorWithRed:175/255.f green:12/255.f blue:6/255.f alpha:1.0];
                             gradiantView.layer.shadowColor = [UIColor colorWithRed:175/255.f green:12/255.f blue:6/255.f alpha:1.0].CGColor;
                             gradiantView.layer.shadowOpacity = 10.0f;
                         }
                         completion:^(BOOL finished){
                         }];
    }
    else {
        startBtn.selected = NO;
        [startBtn setTitle:@"Start Recording" forState:UIControlStateNormal];
        i = 0;
        
        [self stopUpdateTimer];
    }
    gradiantView.layer.cornerRadius = gradiantView.frame.size.width/2;
    gradiantView.layer.borderColor  = [UIColor lightGrayColor].CGColor;
    gradiantView.layer.borderWidth  = i;
}

@end
