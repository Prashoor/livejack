//
//  RTSPPlayerPro.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 18/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "RTSPPlayerPro.h"
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "AlertBox.h"

@interface RTSPPlayerPro () <FFAVPlayerControllerDelegate> {
    FFAVPlayerController *_avplayController;
    
    
    NSArray *_videoAspectRatios;
    NSInteger _videoAspectRatioIndex;
    
    BOOL _hudVisible;
        // Interruption handler
    BOOL _isPlayingBeforeInterrupted;
    
    // Muted handler
    BOOL _isMuted;
}
@end

@implementation RTSPPlayerPro

// Initialize self

- (void)setMediaURL:(NSURL *)mediaURL {
    _mediaURL = mediaURL;
    [self commonInit];
}

- (void)commonInit {
    
    // Other initalization
    _hudVisible = YES;
    _isPlayingBeforeInterrupted = NO;
    _isMuted = NO;
    
    _videoAspectRatioIndex = 0;
    _videoAspectRatios = @[ @(0),       // Default
                            @(-1),      // FULL Screen
                            @(4/3.0f),  // 4:3
                            @(5/4.0f),  // 5:4
                            @(16/9.0f), // 16:9
                            @(16/10.0f),// 16:10
                            @(2.21/1.0f)// 2.21:1
                            ];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleAudioSessionInterruption:)
     name:AVAudioSessionInterruptionNotification
     object:[AVAudioSession sharedInstance]];
    
    // Re-active audio session
    [self toggleAudioSession:YES];
    _avplayController = [[FFAVPlayerController alloc] init];
    _avplayController.delegate = self;
    _avplayController.allowBackgroundPlayback = YES;
    _avplayController.shouldAutoPlay = (_getStartTime == nil ? YES : NO);
    NSMutableDictionary *options = [NSMutableDictionary new];
    
    if (!self.mediaURL.isFileURL) {
        options[AVOptionNameAVProbeSize] = @(256*1024); // 256kb, default is 5Mb
        options[AVOptionNameAVAnalyzeduration] = @(1);  // default is 5 seconds
        options[AVOptionNameHttpUserAgent] = @"Mozilla/5.0";
    }
    
    if (self.avFormatName) {
        options[AVOptionNameAVFormatName] = self.avFormatName;
    }
    
    [_avplayController openMedia:self.mediaURL withOptions:options];
}

- (void)dealloc {
    // Unregister all notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - FFAVPlayerControllerDelegate

- (void)FFAVPlayerControllerWillLoad:(FFAVPlayerController *)controller {
    
}

- (void)FFAVPlayerControllerDidLoad:(FFAVPlayerController *)controller error:(NSError *)error {
    if (error == nil) {
        if (![_avplayController hasVideo]) {
            if ([_avplayController hasAudio]) {
            }
        }
       
        if (_didLoadVideo) {
            _didLoadVideo(_avplayController);
        }
        
        if (_getStartTime) {
            NSNumber *initCurTime =
            _getStartTime(_avplayController, self.mediaURL);
            if (initCurTime.floatValue != CGFLOAT_MAX) {
                [self startPlaybackAt:initCurTime];
            }
        }
    } else {
        [AlertBox showAlert:@"Failed to load video!" confirm:nil];
    }
}

// AVPlayer state was changed
- (void)FFAVPlayerControllerDidStateChange:(FFAVPlayerController *)controller {
    AVPlayerState state = [controller playerState];
    
    if (state == kAVPlayerStateFinishedPlayback) {
        
        // For local media file source
        // If playback reached to end, we return to begin of the media file,
        // and pause the palyer to prepare for next playback.
        
        if ([self.mediaURL isFileURL]) {
            [controller seekto:0];
            [controller pause];
        }
        if (_didFinishPlayback) {
            _didFinishPlayback(controller);
        }
    }
    
    if (state == kAVPlayerStatePlaying) {
        _isPlayingBeforeInterrupted = YES;
        
        // Active audio session
        [self toggleAudioSession:YES];
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        
        // HIde loading
    } else {
        _isPlayingBeforeInterrupted = NO;
        
        if (state == kAVPlayerStatePaused) {
            // De-active audio session
            [self toggleAudioSession:NO];
        }
        
        // Disable idle timer
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    }
    
    // Notify user to save current playback progress
    if (state == kAVPlayerStatePaused || state == kAVPlayerStateStoped ||
        state == kAVPlayerStateFinishedPlayback) {
        
        if (_saveProgress) {
            _saveProgress(controller);
        }
    }
}

// AVPlayer current play time was changed
- (void)FFAVPlayerControllerDidCurTimeChange:(FFAVPlayerController *)controller
                                    position:(NSTimeInterval)position {
}

// AVPlayer current subtitle item was changed
- (void)FFAVPlayerControllerDidSubtitleChange:(FFAVPlayerController *)controller
                                 subtitleItem:(FFAVSubtitleItem *)subtitleItem {
    NSLog(@"%@", subtitleItem);
}

// Enter or exit full screen mode
- (void)FFAVPlayerControllerDidEnterFullscreenMode:
(FFAVPlayerController *)controller {
    // Update full screen bar button
    //  [_fullscreenButton setImage:[UIImage imageNamed:@"avplayer.bundle/zoomout"]];
}

- (void)FFAVPlayerControllerDidExitFullscreenMode:
(FFAVPlayerController *)controller {
    // Update full screen bar button
    //  [_fullscreenButton setImage:[UIImage imageNamed:@"avplayer.bundle/zoomin"]];
}

- (void)FFAVPlayerControllerDidBufferingProgressChange:(FFAVPlayerController *)controller progress:(double)progress {
    // progress is in range [0 ~ 1]
    // Buffering progress changed (total buffer is 5Mb)
}

- (void)FFAVPlayerControllerDidBitrateChange:(FFAVPlayerController *)controller bitrate:(NSInteger)bitrate {
    // Bitrate changed
}

// real framerate was changed
- (void)FFAVPlayerControllerDidFramerateChange:(FFAVPlayerController *)controller framerate:(NSInteger)framerate {
    // Log the bitrate info
    // NSLog(@"framerate : %ld", framerate);
    
#if 0
    /*
     * below codes are just for network playback situation.
     * so you can discard below codes for local playback situation.
     */
    if (framerate > controller.avFramerate/2) {
        if (_loadingIndicatorView.isAnimating) {
            [_loadingIndicatorView stopAnimating];
        }
    } else if ([controller realtimeBitrate] < controller.avBitrate/2 ||
               [controller bufferingProgress] < DBL_EPSILON) {
        if (!_loadingIndicatorView.isAnimating) {
            [self.view bringSubviewToFront:_loadingIndicatorView];
            [_loadingIndicatorView startAnimating];
        }
    }
#endif
}

// query subtitle's encoding
- (CFStringEncoding)FFAVPlayerControllerQuerySubtitleEncoding:(FFAVPlayerController *)controller subtitleCString:(const char *)subtitleCString {
    return kCFStringEncodingGB_18030_2000;
}

// error handler
- (void)FFAVPlayerControllerDidOccurError:(FFAVPlayerController *)controller error:(NSError *)error {
    NSInteger errcode = error.code;
    if (errcode == EIO ||
        errcode == ENETDOWN ||
        errcode == ENETUNREACH ||
        errcode == ENETRESET ||
        errcode == ECONNABORTED ||
        errcode == ECONNRESET ||
        errcode == ENOTCONN ||
        errcode == ETIMEDOUT ||
        errcode == AVPErrorFFmpegEOF ||
        errcode == AVPErrorFFmpegHttpServerError ||
        errcode == AVPErrorFFmpegHttpOther4XX) {
        NSLog(@"SOME NETWORK ERRORs OCCURED, you can do some recover work here...");
    }
}

#pragma mark - AVAudioSession Manager

// Active or de-active audio session with playback category
- (void)toggleAudioSession:(BOOL)enable {
    NSError *error = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    if (enable) {
        /* Set audio session to mediaplayback */
        if (![audioSession setCategory:AVAudioSessionCategoryPlayback
                                 error:&error]) {
            NSLog(@"AVAudioSession (failed to setCategory (%@))", error);
        }
        
        /* active audio session */
        if (![audioSession setActive:YES error:&error]) {
            NSLog(@"AVAudioSession (failed to setActive (YES) (%@))", error);
        }
    } else {
        /* deactive audio session */
        if (![audioSession setActive:NO error:&error]) {
            NSLog(@"AVAudioSession (failed to setActive (NO) (%@))", error);
        }
    }
}

#pragma mark - Audio Session Interruption Handle

- (void)handleAudioSessionInterruption:(NSNotification *)notification {
    NSDictionary *userinfo = [notification userInfo];
    NSUInteger interruptionState = [userinfo[AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
    
    switch (interruptionState) {
        case AVAudioSessionInterruptionTypeBegan: {
            BOOL previousFlag = _isPlayingBeforeInterrupted;
            
            // Player began interruption
            [_avplayController beganInterruption];
            
            // Restore flag
            _isPlayingBeforeInterrupted = previousFlag;
            
            // de-active the audio session when the interruption began
            [self toggleAudioSession:NO];
            break;
        }
        case AVAudioSessionInterruptionTypeEnded: {
            // re-active the audio session for playback
            [self toggleAudioSession:YES];
            
            // Player ends interruption
            [_avplayController endedInterruption];
            
            // Resume the player
            if (_isPlayingBeforeInterrupted) {
                [_avplayController resume];
            }
            break;
        }
    }
}

#pragma mark - Remote Control Event Handle

- (void)remoteControlReceivedWithEvent:(UIEvent *)receivedEvent {
    AVPlayerState currentState = _avplayController.playerState;
    
    if (receivedEvent.type == UIEventTypeRemoteControl) {
        switch (receivedEvent.subtype) {
            case UIEventSubtypeRemoteControlPlay:
                if (currentState == kAVPlayerStatePaused) {
                    [_avplayController resume];
                }
                break;
            case UIEventSubtypeRemoteControlPause:
                if (currentState == kAVPlayerStatePlaying) {
                    [_avplayController pause];
                }
                break;
            case UIEventSubtypeRemoteControlStop:
                _avplayController = nil;
                break;
            case UIEventSubtypeRemoteControlTogglePlayPause:
                if (currentState == kAVPlayerStatePlaying) {
                    [_avplayController pause];
                } else {
                    [_avplayController resume];
                }
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
                // play previous track...
                break;
            case UIEventSubtypeRemoteControlNextTrack:
                // play next track...
                break;
            default:
                break;
        }
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)playDidTouch:(id)sender {
    AVPlayerState playerState = [_avplayController playerState];
    
    if (playerState == kAVPlayerStatePlaying)
        [_avplayController pause];
    else
        [_avplayController resume];
}

- (void)forwardDidTouch:(id)sender {
    NSTimeInterval current_time = [_avplayController currentPlaybackTime];
    NSTimeInterval duration = [_avplayController duration];
    
    [_avplayController seekto:(current_time / duration + 0.05) * duration];
}

- (void)rewindDidTouch:(id)sender {
    NSTimeInterval current_time = [_avplayController currentPlaybackTime];
    NSTimeInterval duration = [_avplayController duration];
    
    [_avplayController seekto:(current_time / duration - 0.05) * duration];
}

- (void)mutedDidTouch:(id)sender {
    _isMuted = !_isMuted;
    [_avplayController setMuted:_isMuted];
}


- (void)speedDidChange:(UIStepper *)sender {
    _avplayController.playbackSpeed = sender.value;
}

- (void)pause {
    [_avplayController pause];
}

/*
 [_avplayController.audioTracks enumerateObjectsUsingBlock:^(NSDictionary *obj,
 NSUInteger idx,
 BOOL *stop) {
 NSString *streamTitle = obj[@"title"];
 NSString *langCode = obj[@"language"];
 
 streamTitle = [streamTitle stringByTrimmingCharactersInSet:cs];
 langCode = [langCode stringByTrimmingCharactersInSet:cs];
 
 NSString *buttonTitle = @"Unknown";
 if ([streamTitle length] > 0) {
 buttonTitle = streamTitle;
 } else if ([langCode length] > 0) {
 NSString *enLangName =
 [FFAVPlayerController convertISO639LanguageCodeToEnName:langCode];
 buttonTitle = enLangName;
 }
 [buttonTitles addObject:[NSString stringWithFormat:@"Track %@ - %@",
 @(idx + 1), buttonTitle]];
 }];
 */

- (void)volumeDidChange:(UISlider *)sender {
    [FFAVPlayerController setVolume:sender.value];
}

#pragma mark - Public

// Start playback at special time position
- (void)startPlaybackAt:(NSNumber *)startTimePosition {
    if (startTimePosition.floatValue == 0) {
        [_avplayController play:0];
    } else {
        double position = startTimePosition.floatValue;
        
        if (![_avplayController play:position]) {
            [_avplayController play:0];
        }
    }
}

@end
