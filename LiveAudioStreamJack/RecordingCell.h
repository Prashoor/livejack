//
//  RecordingCell.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 14/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordingCell : UICollectionViewCell
{
    IBOutlet UISlider *volumeSlider;
    IBOutlet UIButton *muteBtn;
    CGFloat vol;
}
@property (nonatomic, strong) IBOutlet UILabel *connectedUserNm;
- (void)maskForLevel:(CGFloat)level;

@end