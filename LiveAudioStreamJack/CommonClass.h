//
//  CommonClass.h
//  LiveAudioStreamJack
//
//  Created by Nipun Vyas on 25/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonClass : NSObject

@property (nonatomic) NSString *userEmail;

+ (CommonClass *)sharedInstance;

@end
