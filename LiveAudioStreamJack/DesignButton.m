//
//  DesignButton.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 13/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "DesignButton.h"

@implementation DesignButton

- (void)drawRect:(CGRect)rect {
    
    CGContextRef cont = UIGraphicsGetCurrentContext();
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRoundedRect(path, nil, rect, 12, 10);
    
    CGColorRef col = [_backColor colorWithAlphaComponent:0.75f].CGColor;
    if (!col) {
        col = [[UIColor redColor] colorWithAlphaComponent:0.8f].CGColor;
    }
    
    CGContextSetStrokeColorWithColor(cont, [UIColor whiteColor].CGColor);
    CGContextSetFillColorWithColor(cont, col);
    CGContextAddPath(cont, path);
    CGContextFillPath(cont);
}

- (void)setBackColor:(UIColor *)backColor {
    _backColor = backColor;
    [self setNeedsDisplay];
}

@end
