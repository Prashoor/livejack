//
//  RegistrationViewController.m
//  LiveAudioStreamJack
//
//  Created by Nipun Vyas on 24/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "RegistrationViewController.h"
#import "APICall.h"
#import "CommonClass.h"
#import "AlertBox.h"

@interface RegistrationViewController ()
{
    IBOutlet UITextField *firstName;
    IBOutlet UITextField *lastName;
    IBOutlet UITextField *userEmail;
    IBOutlet UITextField *password;
    
    IBOutlet UIButton *cancelBtn;
    IBOutlet UIButton *signUPBtn;
}
@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    cancelBtn.layer.cornerRadius = 8.0;
    cancelBtn.layer.borderColor  = [UIColor whiteColor].CGColor;
    cancelBtn.layer.borderWidth  = 2.0;
    
    signUPBtn.layer.cornerRadius = 8.0;
    signUPBtn.layer.borderColor  = [UIColor whiteColor].CGColor;
    signUPBtn.layer.borderWidth  = 2.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancleRegistration:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)registerUser:(id)sender {
    NSString *msg = @"";
    
    if (![self stringIsValidEmail:userEmail.text]) {
        [AlertBox showAlert:@"Please enter a valid email address" confirm:nil];
        return;
    }
    
    if (firstName.text.length == 0) {
        if (msg.length == 0) {
            msg = @"Please enter first name";
        }
        else {
            msg = [NSString stringWithFormat:@"%@, first name", msg];
        }
    }
    if (lastName.text.length == 0) {
        if (msg.length == 0) {
            msg = @"Please enter last name";
        }
        else {
            msg = [NSString stringWithFormat:@"%@, last name", msg];
        }
    }
    if (userEmail.text.length == 0) {
        if (msg.length == 0) {
            msg = @"Please enter email address";
        }
        else {
            msg = [NSString stringWithFormat:@"%@, email address", msg];
        }
    }
    if (password.text.length == 0) {
        if (msg.length == 0) {
            msg = @"Please enter user password";
        }
        else {
            msg = [NSString stringWithFormat:@"%@, password", msg];
        }
    }
    
    if (msg.length > 0) {
        [AlertBox showAlert:msg confirm:nil];
        return;
    }
    
    APICall *helper = [[APICall alloc] init];
    
    NSMutableDictionary *userdict = [NSMutableDictionary dictionary];
    
    [userdict setObject:firstName.text forKey:@"firstname"];
    [userdict setObject:lastName.text forKey:@"lastname"];
    [userdict setObject:userEmail.text forKey:@"email"];
    [userdict setObject:password.text forKey:@"password"];
    [helper callMethod:RegisterUser andParams:userdict withCallBack:^(NSError *err, id response) {
        if (!err) {
            [[CommonClass sharedInstance] setUserEmail:userEmail.text];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (BOOL) stringIsValidEmail:(NSString *)checkString {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
