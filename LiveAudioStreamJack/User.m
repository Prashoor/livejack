//
//  User.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 29/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "User.h"
@interface User() <NSCoding>

@end

@implementation User
- (instancetype) init {
    self = [super init];
    if (self) {
        [NSKeyedArchiver setClassName:@"User" forClass:User.class];
    }
    return self;
}

+ (instancetype)myProfile {
    static User * user = nil;
    if (!user) {
        NSData * myProfile = [[NSUserDefaults standardUserDefaults] objectForKey:@"profile"];
        user = [NSKeyedUnarchiver unarchiveObjectWithData:myProfile];
        if (!user) {
            user = [User new];
        }
    }
    
    return user;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.userID forKey:@"userID"];
    [aCoder encodeObject:self.groupid forKey:@"groupid"];
    [aCoder encodeObject:self.firstName forKey:@"firstName"];
    [aCoder encodeObject:self.lastName forKey:@"lastName"];
    [aCoder encodeObject:self.email forKey:@"email"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        self.userID = [aDecoder decodeObjectForKey:@"userID"];
        self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
        self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.groupid = [aDecoder decodeObjectForKey:@"groupid"];
    }
    return self;
}

- (void)saveMyProfile:(NSDictionary *)data {
    [self setUserProfile:data];
    NSData *arch = [NSKeyedArchiver archivedDataWithRootObject:self];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:arch forKey:@"profile"];
    if (![def synchronize]) {
        NSLog(@"Error in saving user profile");
    }
}

- (void)setUserProfile:(NSDictionary *)prof {
    _userID = prof[@"_id"];
    _firstName = prof[@"firstName"];
    _lastName = prof[@"lastName"];
    _email = prof[@"userEmail"];
    _groupid = prof[@"groupid"];
    
}

@end

