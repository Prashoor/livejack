//
//  RecordingCell.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 14/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "RecordingCell.h"

@interface RecordingCell ()
{
    UIView *volumeLevel;
}
@end

@implementation RecordingCell

- (void)awakeFromNib {
    volumeSlider.transform=CGAffineTransformRotate(volumeSlider.transform,270.0/180*M_PI);
    
    
    if (!volumeLevel) {
        volumeLevel = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 170)];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = volumeLevel.bounds;
        gradient.colors = [NSArray arrayWithObjects: (id)[[UIColor redColor] CGColor], (id)[[UIColor yellowColor] CGColor], (id)[[UIColor greenColor] CGColor], nil];
        
        NSArray *gradientLocations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],[NSNumber numberWithFloat:.30],[NSNumber numberWithFloat:0.65], nil];
        
        gradient.locations = gradientLocations;
        volumeLevel.layer.borderColor = [UIColor yellowColor].CGColor;
        volumeLevel.layer.borderWidth = 1.f;
        [volumeLevel.layer insertSublayer:gradient atIndex:0];
        
        UIView * vwPage = [[UIView alloc] initWithFrame:CGRectMake(70, 30, 8, 170)];
        vwPage.backgroundColor = [UIColor blackColor];
        [vwPage addSubview:volumeLevel];
        [self addSubview:vwPage];
    }
}

- (void)maskForLevel:(CGFloat)level {
    if (level > 10.f && level < 0.f) {
        return;
    }
    [volumeLevel.maskView removeFromSuperview];
    UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(0 , 0, 8, 170)];
    
    CAGradientLayer *grd = [CAGradientLayer layer];
    grd.frame = mask.bounds;
    grd.colors = [NSArray arrayWithObjects: (id)[[[UIColor blackColor] colorWithAlphaComponent:0.10f] CGColor], (id)[[UIColor whiteColor] CGColor], nil];
    
    NSArray *grdLoc = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],[NSNumber numberWithFloat:level], nil];
    
    grd.locations = grdLoc;
    mask.backgroundColor = [UIColor clearColor];
    [mask.layer insertSublayer:grd atIndex:0];
    
    [volumeLevel setMaskView:mask];
}

- (IBAction)muteBtn:(UIButton *)sender {
    if (!muteBtn.selected) {
        vol = volumeSlider.value;
        volumeSlider.value = volumeSlider.minimumValue;
        muteBtn.selected = YES;
    }
    else {
        muteBtn.selected = NO;
        volumeSlider.value = vol;
    }
}

- (IBAction)changeSliderValue:(UISlider *)sender {
    if (sender.value > 0) {
        muteBtn.selected = NO;
    }
    else if (sender.value == 0) {
        muteBtn.selected = YES;
    }
}

@end