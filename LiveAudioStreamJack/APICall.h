//
//  APICall.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 24/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, APIMethod) {
    LoginUser,
    RegisterUser,
    GetGroupStatus
};

@interface APICall : NSObject

- (void)callMethod:(APIMethod)method andParams:(NSDictionary *)params withCallBack:(void(^)(NSError* err,id response))callBack;

@end
