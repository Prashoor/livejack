//
//  AlertBox.m
//  LiveAudioStreamJack
//
//  Created by prashoor on 29/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import "AlertBox.h"

@interface AlertBox () {
    UIWindow * window;
    UIWindow *topWindow;
}

@end

@implementation AlertBox

+ (instancetype)sharedBox {
    static AlertBox * box = nil;
    if (!box) {
        box->window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        box->window.rootViewController = [[UIViewController alloc] init];
        box->window.tintColor = [UIApplication sharedApplication].delegate.window.tintColor;
        // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
        box->topWindow = [UIApplication sharedApplication].windows.lastObject;
    }
    return box;
}

+ (void)showAlert:(NSString *)title confirm:(void(^)())onConfirm {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:title preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [AlertBox sharedBox]->window.hidden = YES;
        [AlertBox sharedBox]->window.windowLevel = [AlertBox sharedBox]->topWindow.windowLevel - 2;
        if (onConfirm) {
            onConfirm();
        }
    }]];
    [[AlertBox sharedBox] show:alert];
}

+ (void)showAlert:(NSString *)title withOptions:(NSArray<NSString *> *)options onSelect:(void(^)(int))onSelect{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:title preferredStyle:UIAlertControllerStyleActionSheet];
    for (NSString *option in options) {
        [alert addAction:[UIAlertAction actionWithTitle:option style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[AlertBox sharedBox] hide];
            if (onSelect) {
                onSelect((int)[options indexOfObject:option]);
            }
        }]];
    }
    
}

- (void)hide {
    window.hidden = YES;
    window.windowLevel = topWindow.windowLevel - 2;
}

- (void)show:(UIAlertController *)alert {
    window.windowLevel = [AlertBox sharedBox]->topWindow.windowLevel + 1;
    [window makeKeyAndVisible];
    [window.rootViewController presentViewController:alert animated:YES completion:nil];
}


+ (void)showAlert:(NSString *)title confirm:(void(^)())onConfirm onCancel:(void(^)())cancel {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:title preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[AlertBox sharedBox] hide];
        if (onConfirm) {
            onConfirm();
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [[AlertBox sharedBox] hide];
        if (cancel) {
            cancel();
        }
    }]];
    
    [[AlertBox sharedBox] show:alert];
}

@end
