//
//  AlertBox.h
//  LiveAudioStreamJack
//
//  Created by prashoor on 29/07/16.
//  Copyright © 2016 AppMakers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertBox : NSObject

+ (void)showAlert:(NSString *)title confirm:(void(^)())onConfirm;
+ (void)showAlert:(NSString *)title withOptions:(NSArray<NSString *> *)options onSelect:(void(^)(int))onSelect;
+ (void)showAlert:(NSString *)title confirm:(void(^)())onConfirm onCancel:(void(^)())cancel;

@end
