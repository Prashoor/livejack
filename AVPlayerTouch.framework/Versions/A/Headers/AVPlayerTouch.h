//
//  AVPlayerTouch.h
//  AVPlayerTouch
//
//  Created by apple on 15/10/9.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for tvOSAVPlayerTouch.
FOUNDATION_EXPORT double AVPlayerTouchVersionNumber;

//! Project version string for tvOSAVPlayerTouch.
FOUNDATION_EXPORT const unsigned char AVPlayerTouchVersionString[];

#import <AVPlayerTouch/AVPErrors.h>
#import <AVPlayerTouch/AVOptions.h>
#import <AVPlayerTouch/AVPLicense.h>
#import <AVPlayerTouch/OneAudioPlayer.h>
#import <AVPlayerTouch/NativeAVPlayerController.h>
#import <AVPlayerTouch/FFAVPlayerController.h>
#import <AVPlayerTouch/FFAVParser.h>
#import <AVPlayerTouch/CustomizedAVSource.h>
